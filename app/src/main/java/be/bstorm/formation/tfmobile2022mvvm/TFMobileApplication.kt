package be.bstorm.formation.tfmobile2022mvvm

import android.app.Application
import android.content.Context
import dagger.Provides
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TFMobileApplication: Application() {
}