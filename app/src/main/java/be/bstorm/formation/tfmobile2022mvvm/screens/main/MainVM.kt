package be.bstorm.formation.tfmobile2022mvvm.screens.main

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import be.bstorm.formation.tfmobile2022mvvm.api.RetrofitClient
import be.bstorm.formation.tfmobile2022mvvm.api.dto.UserDTO
import be.bstorm.formation.tfmobile2022mvvm.api.services.UserApi
import be.bstorm.formation.tfmobile2022mvvm.room.DbHelper
import be.bstorm.formation.tfmobile2022mvvm.room.daos.UserDAO
import be.bstorm.formation.tfmobile2022mvvm.room.entities.User
import be.bstorm.formation.tfmobile2022mvvm.screens.main.MainActivity.Companion.TAG
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import java.time.LocalDate
import javax.inject.Inject

@HiltViewModel
class MainVM @Inject constructor(
    application: Application
): ViewModel() {

    private val dao: UserDAO = DbHelper.instance(application).users()
    private var _data = mutableListOf<String>()
    val data: MutableLiveData<List<String>> = MutableLiveData(_data)

    init {
        viewModelScope.launch {
            val api = RetrofitClient.getClient().create(UserApi::class.java)

            api.getUsers().enqueue(object: retrofit2.Callback<List<UserDTO>> {
                override fun onResponse(
                    call: Call<List<UserDTO>>,
                    response: Response<List<UserDTO>>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            _data.addAll(it.map { user -> "${user.username} ${user.password}" })
                            data.value = _data
                        }
                    }
                }

                override fun onFailure(call: Call<List<UserDTO>>, t: Throwable) {
                    Log.i(TAG, "onFailure: ${t.message}")
                }

            })

            dao.findAll().collect {
                _data.addAll(it.map { user -> "${user.username} ${user.password}" })
                data.value = _data
            }
        }
    }

    fun addTraduction(value: String) {
        _data.add(value)
        data.value = _data
    }

    fun addUser(user: User) {
        viewModelScope.launch {
            user.createdAt = LocalDate.now();
            dao.create(user)
            _data.add("${user.username} ${user.password}")
            data.value = _data
        }
    }
}