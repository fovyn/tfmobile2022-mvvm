package be.bstorm.formation.tfmobile2022mvvm.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import be.bstorm.formation.tfmobile2022mvvm.room.entities.User
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDAO {
    @Query(value = "SELECT * FROM users")
    fun findAll(): Flow<List<User>>
    @Query(value = "SELECT * FROM users WHERE id = :id")
    fun findById(id: Long): Flow<User>

    @Query(value= "SELECT * FROM users WHERE username = :username")
    fun findByUsername(username: String): Flow<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun create(obj: User): Long
}