package be.bstorm.formation.tfmobile2022mvvm.api.dto

import com.google.gson.annotations.SerializedName

data class UserDTO(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("username")
    var username: String? = null,
    @SerializedName("password")
    var password: String? = null
) {
}