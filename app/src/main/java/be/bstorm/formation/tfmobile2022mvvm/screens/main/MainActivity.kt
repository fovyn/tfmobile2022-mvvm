package be.bstorm.formation.tfmobile2022mvvm.screens.main

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import be.bstorm.formation.tfmobile2022mvvm.adapters.GreetingAdapter
import be.bstorm.formation.tfmobile2022mvvm.databinding.ActivityMainBinding
import be.bstorm.formation.tfmobile2022mvvm.room.entities.User
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), GreetingAdapter.GreetingActionCallback {
    private val vm: MainVM by viewModels()
    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    private val items: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.add.setOnClickListener(this::add)
        binding.items.layoutManager = LinearLayoutManager(this@MainActivity)
        binding.items.adapter = GreetingAdapter(this@MainActivity, items,this@MainActivity)
    }

    override fun onResume() {
        super.onResume()

        vm.data.observe(this@MainActivity) {
            items.clear()
            items.addAll(it)
            Log.i(TAG, "onResume: $it => ${items.size}")
            binding.items.adapter?.notifyDataSetChanged()
        }
    }

    private fun add(v: View) {
        vm.addUser(User("Flavian", "Ovyn"))
    }

    companion object {
        val TAG = MainActivity::class.java.name
    }

    override fun greetingBtnAction(view: GreetingAdapter.GreetingVH) {
        Log.i(TAG, "greetingBtnAction: ${view.greeting.text}")
    }
}