package be.bstorm.formation.tfmobile2022mvvm.room.converters

import androidx.room.TypeConverter
import java.time.LocalDate

class LocalDateConverter {

    @TypeConverter
    fun Long?.toLocalDate(): LocalDate? {
        if (this != null) return LocalDate.ofEpochDay(this)
        return null;
    }

    @TypeConverter
    fun LocalDate?.toLong(): Long? {
        return this?.toEpochDay()
    }
}