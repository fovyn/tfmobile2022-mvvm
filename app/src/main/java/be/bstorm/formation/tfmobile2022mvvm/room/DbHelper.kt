package be.bstorm.formation.tfmobile2022mvvm.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import be.bstorm.formation.tfmobile2022mvvm.room.daos.UserDAO
import be.bstorm.formation.tfmobile2022mvvm.room.entities.User

@Database(entities = [User::class], version = 1, exportSchema = false)
abstract class DbHelper: RoomDatabase() {

    abstract fun users(): UserDAO

    companion object {
        const val DB_NAME = "room_database"
        private var instance: DbHelper? = null

        fun instance(context: Context): DbHelper {
            if (instance == null) {
                instance = Room
                    .databaseBuilder(context, DbHelper::class.java, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance!!
        }
    }
}