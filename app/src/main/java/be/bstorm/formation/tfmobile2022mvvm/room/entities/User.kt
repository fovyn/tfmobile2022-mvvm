package be.bstorm.formation.tfmobile2022mvvm.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import be.bstorm.formation.tfmobile2022mvvm.room.converters.LocalDateConverter
import java.time.LocalDate

@Entity(
    tableName = "users"
)
@TypeConverters(value = [LocalDateConverter::class])
data class User constructor(
    var username: String,
    var password: String,
    var createdAt: LocalDate? = null,
    var updatedAt: LocalDate? = null,
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
): java.io.Serializable{
}