package be.bstorm.formation.tfmobile2022mvvm.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.bstorm.formation.tfmobile2022mvvm.R

class GreetingAdapter(
    private val context: Context,
    private val list: List<String>,
    private val callback: GreetingActionCallback? = null
): RecyclerView.Adapter<GreetingAdapter.GreetingVH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GreetingVH {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_greeting, parent, false)

        Log.i(TAG, "onCreateViewHolder: START")
        return GreetingVH(view)
    }

    override fun onBindViewHolder(holder: GreetingVH, position: Int) {
        Log.i(TAG, "onBindViewHolder: $position")
        holder.greeting.text = list[position]
        callback?.also {
            holder.btnAction.setOnClickListener { callback.greetingBtnAction(holder) }
        }
    }

    override fun getItemCount(): Int = list.size


    inner class GreetingVH(view: View): RecyclerView.ViewHolder(view) {
        val greeting: TextView = view.findViewById(R.id.greeting)
        val btnAction: Button = view.findViewById(R.id.btnAction)
    }

    interface GreetingActionCallback {
        fun greetingBtnAction(view: GreetingVH): Unit
    }

    companion object {
        val TAG = GreetingAdapter::class.java.name
    }
}