package be.bstorm.formation.tfmobile2022mvvm.api.services

import be.bstorm.formation.tfmobile2022mvvm.api.dto.UserDTO
import retrofit2.Call
import retrofit2.http.GET

interface UserApi {
    @GET("users")
    fun getUsers(): Call<List<UserDTO>>
}